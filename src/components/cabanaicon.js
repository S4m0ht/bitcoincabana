import React from "react"
import cabanaPNG from "../images/bitcoincabana-icon.png"

/*
 * This component is built using `gatsby-image` to automatically serve optimized
 * images with lazy loading and reduced file sizes. The image is loaded using a
 * `useStaticQuery`, which allows us to load the image from directly within this
 * component, rather than having to pass the image data down from pages.
 *
 * For more information, see the docs:
 * - `gatsby-image`: https://gatsby.dev/gatsby-image
 * - `useStaticQuery`: https://www.gatsbyjs.org/docs/use-static-query/
 */

const CabanaIcon = () => {

  return <img src={cabanaPNG} alt="Bitcoin Cabana Podcast" style={{ width: '30px', height: '30px', margin: 0 }}/>
}

export default CabanaIcon