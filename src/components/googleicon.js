import React from "react"
import googleSVG from "../images/google.svg"

/*
 * This component is built using `gatsby-image` to automatically serve optimized
 * images with lazy loading and reduced file sizes. The image is loaded using a
 * `useStaticQuery`, which allows us to load the image from directly within this
 * component, rather than having to pass the image data down from pages.
 *
 * For more information, see the docs:
 * - `gatsby-image`: https://gatsby.dev/gatsby-image
 * - `useStaticQuery`: https://www.gatsbyjs.org/docs/use-static-query/
 */

const GoogleIcon = () => {

  return <img src={googleSVG} alt="Google Podcast" width="18" style={{marginTop:'2px', marginRight:'3px'}}/>
}

export default GoogleIcon